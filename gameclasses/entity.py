from gameclasses.location import Location2D, Location3D


class Entity2D:
	def __init__(self, x, y):
		self.loc = Location2D(x, y)
		self.type = 'ENTITY'


	def getLocation(self):
		return self.loc


	def getPlayerLocation(self):
		return self.loc.getLocation()


class Entity3D:
	def __init__(self, x, y, z):
		self.loc = Location3D(x, y, z)
		self.type = 'ENTITY'


	def getLocation(self):
		return self.loc


	def getPlayerLocation(self):
		return self.getLocation()