from gameclasses.inventory import Inventory
from gameclasses.location import Location2D, Location3D


class Player2D:
	def __init__(self, x, y):
		self.loc = Location2D(x, y)
		self.inv = Inventory(32)
		self.type = 'PLAYER'


	def getLocation(self):
		return self.loc


	def getPlayerLocation(self):
		return self.loc.getLocation()


class Player3D:
	def __init__(self, x, y, z):
		self.loc = Location3D(x, y, z)
		self.inv = Inventory(32)
		self.type = 'PLAYER'
	

	def getLocation(self):
		return self.loc


	def getPlayerLocation(self):
		return self.loc.getLocation()