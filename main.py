from database import *
from gameclasses.player import Player2D, Player3D
from gameclasses.entity import Entity2D, Entity3D
from gameclasses.location import Location2D, Location3D
from gameclasses.inventory import Inventory

if __name__ == "__main__":
    player = Player2D(0, 0)
    pig = Entity2D(0, 0)